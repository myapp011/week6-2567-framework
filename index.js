const  http = require('http');
const app = http.createServer((req,res) => {
    if(req.url === '/'){
    res.writeHead(200,{"Content-Type": "Text/html"});
    res.write("<h3>Hello world</h3>");
    res.end();
    }else if (req.url === "/about"){
    res.writeHead(200, {"Content-Type": "text/html"})
    res.write("<h3>My name is Thanawat Phalaket</h3>");
    res.end();
    }else if (req.url === "/admin"){
    res.writeHead(200, {"Content-Type": "text/html"})
    res.write("<h3>Admin page</h3>");
    res.end();
    }else if (req.url === "/Home"){
    res.writeHead(200, {"Content-Type": "text/html"})
    res.write("<h3>Home page</h3>");
    res.end();
    }
});

const PORT = 3000
const hostname = 'gaybu';
app.listen(PORT,  ()=>{
    console.log(`Server running at http://${hostname}:${PORT}`);
});
